from django.db import models


class Student(models.Model):
    first_name  = models.CharField(max_length=70)
    last_name   = models.CharField(max_length=70)
    age         = models.SmallIntegerField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    
    