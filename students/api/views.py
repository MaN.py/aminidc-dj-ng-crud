from rest_framework import generics

from students.models import Student
from .serializers import StudentSerializer


class StudentListCreateAPIView(generics.ListCreateAPIView):
    '''
    List and create students endpoint.

    Get: return list all of the students.
    Post: create a new student.
    '''

    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    '''
    Manage student with ID endpoint.

    Get: return a student.
    Put: update a student.
    Delete: delete a student.
    '''

    queryset = Student.objects.all()
    serializer_class = StudentSerializer




# Other mode endpoints

# --ApiView

# class StudentListAPIView(APIView):

#     def get(self, request, format=None):
#         """Return a list of all students."""

#         queryset = Student.objects.all()
#         slz_students = StudentSerializer(queryset, many=True).data
#         return Response({"students": slz_students})

#     def post(self, request, format=None):
#         """Create a student."""

#         student = request.data.get('student')

#         # Create an student from the above data
#         serializer = StudentSerializer(data=student)
#         if serializer.is_valid(raise_exception=True):
#             student_saved = serializer.save()
#         return Response({"success": "student created successfully"})


# -- GenericViews

# class StudentListAPIView(generics.ListAPIView):
#     """Return list all of the students."""

#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer


# class StudentRetrieveAPIView(generics.RetrieveAPIView):
#     """Return a single student."""

#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer


# class StudentUpdateAPIView(generics.UpdateAPIView):
#     """Update a single student."""

#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer


# class StudentDestroyAPIView(generics.DestroyAPIView):
#     """Delete a single student."""

#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer


# class StudentCreateAPIView(generics.CreateAPIView):
#     """Create a single student."""

#     queryset = Student.objects.all()
#     serializer_class = StudentSerializer

